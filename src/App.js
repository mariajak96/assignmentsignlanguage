import './App.css';

import {
  BrowserRouter, 
  Routes,
  Route, 
  NavLink,
} from "react-router-dom"
import LogIn from "./View/LogInPage"
import Profile from "./View/ProfilePage"
import Translate from "./View/TranslatePage"
 
function App() {           //Router 
  return ( 
    <BrowserRouter>
        <div className="App">
          <Routes>
            <Route path="/LogIn" element={<LogIn/>} />
            <Route path="/Translate" element={<Translate/>} />
            <Route path="/ProfilePage" element={<Profile/>} />
          </Routes> 
        </div>
    </BrowserRouter>
  );
}

export default App;

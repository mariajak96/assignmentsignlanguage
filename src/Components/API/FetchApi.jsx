import { useState, useEffect } from "react"; 
import { useState } from "react";
import { useForm } from 'react-hook-form';


const LoginForm = () => {
  // URL stored in a variable for easier use.   
const apiURL = 'https://noroff-api-translate-production.up.railway.app/'
 

//local states here
     const [username, setUsername] = useState(""); 
     const navigate = useNavigate()


    const handleSubmitLogIn = async (event) => {
    event.preventDefault(); // Prevents changes bizarre errors from happening
    
    // Get request checks if username in the API matches the entered username
    const response = await fetch(`${apiURL}/translations?username=${username}`,{method: 'Get'}) 
    
    // Responses that are datastream are converted to json format (that returns an array with the reqested data)
    const data = await response.json();
    
    // If array is not empty -> usesrname in the Api that matches the text in input. Then the user is directed to translation page. 
    if (data.length > 0){
        console.log(data)
        navigate('/translate')
     }
     else {
        console.log("Username is not registered");
     }       
}

 return( //OnSubmit: when the user hits the log in button. 
         //HandleSubmitlogIn: carries out when OnSubmit takes action. 
         // State is updated whenever it checks if username exists. 
    <form onSubmit={handleSubmitLogIn}> 
    <input class="logInInputField" type="text" value= {username} onChange={(e)=> setUsername(e.target.value)}/>
    
    <button class=" btn logInBtn" type="submit"> Log in </button>
    </form>
 )   
}

export default LoginForm; 

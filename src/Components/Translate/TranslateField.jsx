import parse from 'html-react-parser';
import { createElement } from 'react';
import { useState } from "react";

const TranslateField  = () => {
    let APIfetch = []; 
}

//Backup solution for our trouble with the API not completing the fetch before the rest of the program is running. 
const ArraySignsBackup = [
    { id: "a", 
    ref: "/Signs/a.png"},
  
    { id: "b", 
    ref: "/Signs/b.png"},
  
    { id: "c", 
    ref: "/Signs/c.png"},
  
    { id: "d", 
    ref: "/Signs/d.png"},
  
    { id: "e", 
    ref: "/Signs/e.png"},
  
    { id: "f", 
    ref: "/Signs/f.png"},
  
    {id: "g", 
    ref: "/Signs/g.png"},
  
    {id: "h", 
    ref: "/Signs/h.png"},
  
    {id: "i", 
    ref: "/Signs/i.png"},
  
    {id: "j", 
    ref: "/Signs/j.png"},
  
    {id: "k", 
    ref: "/Signs/k.png"},
  
    {id: "l", 
    ref: "/Signs/l.png"},
  
    {id: "m", 
    ref: "/Signs/m.png"},
  
    {id: "n", 
    ref: "/Signs/n.png"},
  
    {id: "o", 
    ref: "/Signs/o.png"},
  
    {id: "p", 
    ref: "/Signs/p.png"},
  
    {id: "q", 
    ref: "/Signs/q.png"},
  
    {id: "r", 
    ref: "/Signs/r.png"},
  
    {id: "s", 
    ref: "/Signs/s.png"},
  
    {id: "t", 
    ref: "/Signs/t.png"},
  
    {id: "u", 
    ref: "/Signs/u.png"},
  
    {id: "v", 
    ref: "/Signs/v.png"},
  
    {id: "w", 
    ref: "/Signs/w.png"},
  
    {id: "x", 
    ref: "/Signs/x.png"},
  
    {id: "y", 
    ref: "/Signs/y.png"},
  
    {id: "z", 
    ref: "/Signs/z.png"}         
   ];
 
   const APILink = 'https://noroff-api-translate-production.up.railway.app/'

   //Local state here. 
   const [translateInput, setTranslateInput] = useState("");
   const getSignArray = (event) =>{

    //Not being used atm. 
    const response = /*await */fetch(`${APILink}/signs`)
    const data = /*await*/ response.json();
    if (data.length > 0){
    // we have commented out this part because of the APIfetch  is linked to ArraySignsBackup

    return APIfetch 
}
else console.log("else");

}
APIfetch  = ArraySignsBackup; 

//Convert the text in the input field to an array of characters. 
let arrayNY = Array.from(translateInput)

//When the user enters its username: store the file paths of the characters. 
let userInputF= [];

// Loop through the entered text - comp the characters to ArrayBackupPlanSign. Then save in userInputF array. 
arrayNY.forEach(element => {
    for (let i = 0; i < APIfetch .length; i++){
       if (element == APIfetch [i].id){ 
        userInputF.push (APIfetch [i].ref)  
       }  
    }
 });
 
 // Storing this in a variable result.
let result = userInputF.map(i => 
    <img src= {i} />)
    
//Not working atm. Button does not save the translations. 
return( 
    <div> 
    <form onSubmit={getSignArray}>
             <input type="text" value= {translateInput}onChange={(e) => setTranslateInput(e.target.value)} /> 
             <button class= "btn translateBtn" type='submit'> Save the translation </button>
          </form>
    <p>{result} </p>
       
    </div>
)

export default TranslateField;

/* 
return (
    <div>
        <input/>
        <button class="btn translateBtn">Translate</button>
        <section class="TranslateFieldOutput"></section>
    </div>
    )
*/
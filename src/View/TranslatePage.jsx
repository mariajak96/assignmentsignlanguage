import NavBar from "../Components/NavBarFolder/navbar";
import TranslateField from "../Components/Translate/TranslateField";


const TranslatePage = () => {  
    return ( //return so that the h-tag is shown.
    <div> 
        <NavBar/>
        <h1 class="welcomeH1"> 
        Welcome, what do you wish to translate?
        </h1>

        <TranslateField/>
    </div> 

    )
}

export default TranslatePage